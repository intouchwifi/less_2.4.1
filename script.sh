#!/bin/bash

curl --request POST --header "PRIVATE-TOKEN: ZR93Bj76kTv8DP5syc7B" \
--form "variables[][key]=MY_IMAGE_APP" \
--form "variables[][value]=$MY_IMAGE_APP" \
--form "variables[][key]=MY_IMAGE_NGINX" \
--form "variables[][value]=$MY_IMAGE_NGINX" \
--form "variables[][key]=MY_PORT" \
--form "variables[][value]=$MY_PORT" \
--form "variables[][value]=$MY_TOKEN" \
--form "variables[][key]=MY_TOKEN" \
"https://gitlab.com/api/v4/projects/21735255/pipeline?ref=master" \


JOB_ID=$(curl --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/21735255/jobs" | jq '.[0] | select(.name == "start_project") .id')
echo $JOB_ID

curl -k --request POST --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/21735255/jobs/$JOB_ID/play"
